const express = require('express')
const fs = require('fs');
const app = express();
const port = 3000

app.set('view engine', 'ejs');
app.use(express.static('public'))


app.get('/', (req, res) => {
  const limit = 100;
  const page = req.query.page || 1;
  let {name, email, ip, lastName, gender, sortBy} = req.query;
  let url = '';
  Object.keys(req.query).forEach(key => {
    if (key !== 'page') url += `${key}=${req.query[key]}&`;
  });

  let data = JSON.parse(fs.readFileSync('assets/data.json'));
  data = data.sort(function (a, b) {return a.first_name.localeCompare(b.first_name)})
  if(sortBy) {
    data = data.sort(function (a, b) {
      return sortBy == 'asc'? a.gender.localeCompare(b.gender) : b.gender.localeCompare(a.gender)
    });
  }

  if(name) {
    data = data.filter((item) => item.first_name.toLowerCase().indexOf(name.toLowerCase()) > -1 );
  }

  if(email) {
    data = data.filter((item) => item.email.toLowerCase().indexOf(email.toLowerCase()) > -1);
  }

  if(ip) {
    data = data.filter((item) => item.ip_address.toLowerCase().indexOf(ip.toLowerCase()) > -1);
  }

  if(lastName) {
    data = data.filter((item) => item.last_name.toLowerCase().indexOf(lastName.toLowerCase()) > -1);
  }

  if(gender && gender !== '----') {
    data = data.filter((item) => item.gender === gender);
  }
  
  let pageSize =  Math.ceil(data.length / limit);
  
  data = data.slice((page - 1) * limit, page*limit),
  res.render('index',{data, paginationData: {pageSize, page, url}, filterData: {name, lastName, email, ip, gender}});
})



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})